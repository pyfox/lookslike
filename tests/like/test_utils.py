import unittest

from lookslike import Like, utils


class TestUtils(unittest.TestCase):
    def test_filter_keys(self):
        assert {'a': 1, 'b': 2, 'c': 42} == Like({'a': 1, 'b': 2},
                                                 convert=utils.filter_keys(['a', 'b']))
        assert {'a': 1, 'b': 2, 'c': 42} != Like({'a': 1, 'b': 2, 'c': 3},
                                                 convert=utils.filter_keys(['a', 'c']))

    def test_dict_keys_as_set(self):
        assert {'a': 1, 'b': 2, 'c': 3} == Like({'a', 'b', 'c'}, 
                                                convert=utils.dict_keys_as_set())

    def test_is_truthy(self):
        assert 0 != Like(utils.is_truthy())
        assert 1 == Like(utils.is_truthy())
        assert '' != Like(utils.is_truthy())
        assert 'a' == Like(utils.is_truthy())
        assert [] != Like(utils.is_truthy())
        assert [1] == Like(utils.is_truthy())
        assert None != Like(utils.is_truthy())
        assert {} != Like(utils.is_truthy())
        assert {'a': 1} == Like(utils.is_truthy())