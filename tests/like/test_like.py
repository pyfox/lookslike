import re
import sys
import unittest

from lookslike import Like, Similar, utils


class TestLike(unittest.TestCase):
    def test_int_comparison(self):
        assert 42 == Like(42)
        assert 42 == Like(int)
        assert 42 != Like(float)
        assert 42 != Like(str)
        assert 42 == Like(lambda number: 40 < number < 44)
        assert 42 != Like(lambda number: 0 < number < 5)

    def test_similar_strict_type_check(self):
        """Test that the type checks are running even if the object has been locked already."""
        assert 1.0 != Similar(int)
        assert 1 == Similar(int)
        s = Similar(int)
        assert [1, 1.0] != [s, s]  # False due to strict type check for int
        s = Similar()
        assert [1, 1.0] == [s, s]  # True as 1.0 == 1
        assert ["1", 1] != [s, s]  # False as "1" != 1

    def test_unlike_after_init(self):
        """
        Test that the first value of a "Similar" object always "wins".
        """
        l = Similar(int)
        assert l == 42
        assert repr(l) == '42'
        assert not l == 43
        assert repr(l) == '!Similar(locked_to=42)'

        i = Similar(int)
        assert [1, 2] != [i, i]
        assert repr(i) == '!Similar(locked_to=1)'

    def test_repr_similar(self):
        l = Similar(int)
        assert repr(l) == "?Similar(<class 'int'>)"
        l == 42  # noqa
        assert repr(l) == "42"
        l == 43  # noqa
        assert repr(l) == "!Similar(locked_to=42)"
        dictionary = {
            'a': 1,
            'b': 2,
            'nested': {
                'c': 3,
                'd': 4
            }
        }
        comparison = {
            'a': Similar(int),
            'b': Similar(int),
            'nested': {
                'c': Similar(re.compile('3')),
                'd': Similar(str),
            }
        }
        assert dictionary != comparison
        if sys.version_info[:2] != (3, 3):
            assert str(comparison) == "{'a': 1, 'b': 2, 'nested': {'c': 3, 'd': !Similar(<class 'str'>)}}"

    def test_repr(self):
        """
        Test repr(Like).

        This will be called e.g. by pytest to show the differences between 
        to objects.
        Since Pytest does not support nested diff, "Like" repr will have 
        a "!" as prefix when the last comparison has been Falsy.
        """
        l = Like(42)
        assert repr(l) == "?Like(42)"
        l == 42  # noqa
        assert repr(l) == "Like(42)"
        dictionary = {
            'a': 1,
            'b': 2,
            'nested': {
                'c': 3,
                'd': 4
            }
        }
        comparison = {
            'a': Like(int),
            'b': Like(int),
            'nested': {
                'c': Like(re.compile('3')),
                'd': Like(str),
            }
        }
        assert dictionary != comparison
        stringified_comparison = str(comparison)
        if sys.version_info[:2] != (3, 3):
            assert "\'a\': Like(<class \'int\'>)" in stringified_comparison
            assert "\'b\': Like(<class \'int\'>)" in stringified_comparison
            assert "\'d\': !Like(<class \'str\'>)" in stringified_comparison
            assert "\'c\': Like(re.compile(\'3\'))" in stringified_comparison

    def test_regex_comparison(self):
        assert 42 == Like(re.compile('4.'))
        assert 42 != Like(re.compile('4'))
        assert 42 != Like(int, re.compile('4'))
        assert 42.0 == Like(re.compile('42.0'))
        assert 42.0 == Like(re.compile(r'42\.0'))
        assert 42.0 != Like(re.compile('42,0'))
        assert 42.0 == Like(re.compile(r'.*\.0'))
        assert 42.0 == Like(re.compile('4.*'))
        assert 123.456 == Like(re.compile(r"\d+\.\d+"))

    def test_byte_comparison(self):
        assert b'ab' == Like(bytes)
        assert b"ab" == Like(re.compile(b'a.'))
        assert b"ab" != Like(re.compile(b'a'))
        assert b"ab" != Like(re.compile('ab'))

    def test_string_comparison(self):
        assert '42' == Like('42')
        assert '42' == Like(True, convert=bool)
        assert '' != Like(True, convert=bool)

    def test_float_comparison(self):
        assert 42.0 == Like(float)
        assert 42 != Like(float)
        assert 42 != Like(42.0)

    def test_multiple_comparators(self):
        assert '42' == Like(re.compile('4.'))
        assert '42' == Like(str, re.compile('4.'))
        assert '42' != Like(int, re.compile('4.'))

    def test_dict_comparison(self):
        assert {'ab': '42'} == {'ab': Like('42')}
        assert {'ab': 42} == {'ab': Like(int)}
        assert {'ab': 42.0} != {'ab': Like(int)}
        assert {'ab': 42} == Like(dict)
        assert {'one': 1, 'two': 2, 'three': 3} \
               == Like({'one': 1, 'two': 2, 'three': 3})
        assert {'one': 1, 'two': 2, 'three': 3} \
               == Like({'one': 1, 'two': Like(int), 'three': 3})
        assert {'one': 1, 'two': 2, 'three': 3} \
               != Like({'one': 1, 'three': 3})
        assert {'one': 1, 'two': 2, 'three': 3} \
               == Like({'one': 1, 'three': 3},
                       convert=lambda d: {key: value for key, value in d.items() if
                                          key in ['one', 'three']})

    def test_list_comparison(self):
        assert [1, 2, 3] == [1, Like(2), 3]
        assert [1, 2, 3] == [1, Like(int), 3]
        assert [1, 2, 3] != [1, Like(float), 3]
        assert [1, 2, 3] == Like(list)
        assert [1, 2, 3] == Like([1, 2, 3])
        assert [1, 2, 3] != Like([3, 2, 1])
        assert [1, 2, 3] == Like({3, 2, 1}, convert=set)
        assert [3, 2, 1] == Like([1, 2, 3], convert=sorted)

    def test_example(self):
        server_response = {  # This is your test object of course
            'response_id': 42,
            'timestamp': 2134567.2355,
            'pets': ['cat', 'dog', 'chicken'],
            'number_of_pets': 3,
            'info_url': 'https://petsdb.info/',
            'vet name': 'Dr. Murphey',
            "animals_in_shelter": ["eli-123"],
            "animals": {
                "ids": {
                    "id": "eli-123",
                    "name": "dumbo",
                    "age": 42,
                    "type": "elephant"
                }
            },
            'home': {
                'home_id': 1242,
                'address': {
                    'street': 'Rabbit street 42',
                    'city': 'New Bark',
                }
            }
        }

        elephant_id = Similar()
        assert server_response == {
            'response_id': Like(int),
            'timestamp': Like(float),
            'pets': Like(['cat', 'chicken', 'dog'], convert=sorted),
            'number_of_pets': 3,
            'info_url': Like(lambda value: value.startswith('https://')),
            'vet name': Like(re.compile('Dr\. .*')),
            "animals_in_shelter": [elephant_id],
            "animals": {
                "ids": {
                    "id": elephant_id,
                    "name": "dumbo",
                    "age": 42,
                    "type": "elephant"
                }
            },
            'home': {
                'home_id': Like(int),
                'address': Like({'city': 'New Bark'},
                                convert=utils.filter_keys(['city']))
            }
        }
