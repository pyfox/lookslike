import os
import unittest


class TestRelease(unittest.TestCase):
    def test_release(self):
        """
        Tests the release files.

        This checks that the release-specific url's given in the Readme are pointing to the correct release.
        The environment variable RELEASE_VERSION will be set in the pipeline.
        """
        release = os.environ.get('RELEASE_VERSION', '')
        with open(os.path.abspath(__file__+'../../../README.md')) as handler:
            text = handler.read()
        if release.startswith('release/'):
            version = release.replace('release/', '')
            version_pipeline_badge = "https://gitlab.com/pyfox/lookslike/badges/release/{version}/pipeline.svg"\
                .format(version=version)
            branch_url = "https://gitlab.com/pyfox/lookslike/-/tree/release/{version}".format(version=version)

            assert version_pipeline_badge in text
            assert branch_url in text
            assert "## Changes\n\n{version}".format(version=version) in text
