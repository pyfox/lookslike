#!/usr/bin/env python
import os
from distutils.core import setup

from setuptools import find_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

_VERSION = os.environ.get('RELEASE_VERSION', 'develop')
if _VERSION.startswith('release/'):
    _VERSION = _VERSION.replace('release/', '')
else:
    _VERSION = '0.0.0'

setup(name="lookslike",
      version=_VERSION,
      description="Compare dictionaries, lists and other objects "
                  "convenient and readable",
      author="kitsunebi",
      author_email="me@pyfox.net",
      long_description=long_description,
      long_description_content_type="text/markdown",
      url="https://gitlab.com/pyfox/lookslike/",
      project_urls={
          "Bug Tracker": "https://gitlab.com/pyfox/lookslike/-/issues"
      },
      packages=find_packages(where="src"),
      package_dir={"": "src"},
      classifiers=[
          "License :: OSI Approved :: MIT License",
          "Operating System :: OS Independent",
          "Development Status :: 5 - Production/Stable",
          "Environment :: Web Environment",
          "Intended Audience :: Developers",
          "Intended Audience :: System Administrators",
          "Topic :: Software Development :: Testing",
          "Topic :: Software Development :: Quality Assurance",
          "Programming Language :: Python :: 3",
          "Programming Language :: Python :: 3.3",
          "Programming Language :: Python :: 3.4",
          "Programming Language :: Python :: 3.5",
          "Programming Language :: Python :: 3.6",
          "Programming Language :: Python :: 3.7",
          "Programming Language :: Python :: 3.8",
          "Programming Language :: Python :: 3.9",
          "Programming Language :: Python :: 3.10",
          "Programming Language :: Python :: 3.11"
      ],
      )
